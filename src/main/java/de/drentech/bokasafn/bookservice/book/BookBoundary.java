package de.drentech.bokasafn.bookservice.book;

import de.drentech.bokasafn.bookservice.book.model.Book;
import org.eclipse.microprofile.metrics.MetricUnits;
import org.eclipse.microprofile.metrics.annotation.Timed;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.ext.Provider;

@Path("/book")
@Provider
public class BookBoundary {

    @Inject
    private BookFascade bookFascade;

    @GET
    @Produces(MediaType.TEXT_PLAIN)
    public String hello() {
        return "hello";
    }

    @GET
    @Path("/random")
    @Produces(MediaType.APPLICATION_JSON)
    @Timed(unit = MetricUnits.MILLISECONDS)
    public Book generateRandomBook() {
        return this.bookFascade.randomBook();
    }
}