package de.drentech.bokasafn.bookservice.book;

import de.drentech.bokasafn.bookservice.book.model.Book;
import de.drentech.bokasafn.bookservice.book.model.BookRepository;
import org.eclipse.microprofile.metrics.annotation.Counted;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

@ApplicationScoped
public class BookFascade {

    @Inject
    private BookRepository bookRepository;

    @Counted(monotonic = true)
    public Book randomBook() {
        return this.bookRepository.addRandomTask();
    }
}
