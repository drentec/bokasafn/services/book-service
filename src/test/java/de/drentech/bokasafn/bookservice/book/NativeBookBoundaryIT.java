package de.drentech.bokasafn.bookservice.book;

import io.quarkus.test.junit.SubstrateTest;

@SubstrateTest
public class NativeBookBoundaryIT extends BookBoundaryTest {

    // Execute the same tests but in native mode.
}